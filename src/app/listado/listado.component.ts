import { Component, OnInit } from '@angular/core';

import { Estudiante } from './../interfaces/estudiante';

const ESTUDIANTES:Array<Estudiante> = [
  {nombre: 'Maria Perez',legajo:123,materia:'PHP',regular:true},
  {nombre: 'Roberto Gonzalez',legajo:456,materia:'Angular',regular:true},
  {nombre: 'Andrea Ramirez',legajo:789,materia:'C#',regular:true}
]

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {
  estudiantes:Array<Estudiante> = ESTUDIANTES;
  constructor() { }

  ngOnInit(): void {
    console.log("se cargó el componente")
  }

}
